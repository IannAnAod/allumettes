<?php

include 'new_game_2_joueurs.php';
include 'nombre_allumette.php';
include 'tour_de_jeu.php';


allumettes_restantes();

function joueur_vs_joueur() {

    global $nb_allum;
    global $humain;
    global $joueur2;
    
    while ($nb_allum > 0){
        echo PHP_EOL . PHP_EOL;
        echo "\033[32m-- Tour de " . $humain . " --\033[0m" . PHP_EOL . PHP_EOL;
        echo "\033[34mPrends 1 à 3 allumettes : \033[0m";

        prendre_allumette();
        allumettes_restantes();

        if($nb_allum == 0) {
            echo "\033[34m" . $humain . " a perdu !\033[0m" . PHP_EOL;
            break;
        }

        if($nb_allum > 0){
            echo PHP_EOL . PHP_EOL;
            echo "\033[32m-- Tour du " . $joueur2 . " --\033[0m" . PHP_EOL . PHP_EOL;
            echo "\033[34mPrends 1 à 3 allumettes : \033[0m";
            prendre_allumette();
            allumettes_restantes();
            if($nb_allum == 0) {
                echo "\033[34m" . $joueur2 . " a perdu !\033[0m" . PHP_EOL;
                break;
            }
        }
    }
}
joueur_vs_joueur();
