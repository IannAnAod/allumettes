<?php

include 'new_game_1_joueur.php';
include 'nombre_allumette.php';
include 'tour_de_jeu.php';
include 'ai.php';
include 'rejouer.php';



function joueur_vs_ai_1_allumette() {
    global $nb_allum;
    global $humain;
    
    allumettes_restantes();

    while ($nb_allum > 0){
        // Tour du joueur
        echo PHP_EOL . PHP_EOL;
        echo "\033[32m-- Tour de " . $humain . " --\033[0m" . PHP_EOL . PHP_EOL;
        echo "\033[34mPrends 1 à 3 allumettes : \033[0m";

        prendre_allumette();
        allumettes_restantes();

        if($nb_allum == 0) {
            echo "\033[34m" . $humain . " a perdu !\033[0m" . PHP_EOL;
            break;
        }

        // Tour de l'AI
        if($nb_allum > 0){
            echo PHP_EOL . PHP_EOL;
            echo "\033[32m-- Tour de l'A.I. --\033[0m" . PHP_EOL . PHP_EOL;
            tour_ai_1_allumette();
            if($nb_allum == 0) {
                break;
            }
        }
    }

}
// joueur_vs_ai_1_allumette();

function joueur_vs_ai_plusieurs_allumettes() {
    global $nb_allum;
    global $humain;

    allumettes_restantes();
    while ($nb_allum > 0){
        // Tour du joueur
        echo PHP_EOL . PHP_EOL;
        echo "\033[32m-- Tour de " . $humain . " --\033[0m" . PHP_EOL . PHP_EOL;
        echo "\033[34mPrends 1 à 3 allumettes : \033[0m";

        prendre_allumette();
        allumettes_restantes();

        if($nb_allum == 0) {
            echo "\033[34m" . $humain . " a perdu !\033[0m" . PHP_EOL;
            rejouer();
        }

        // Tour de l'AI
        if($nb_allum > 0){
            echo PHP_EOL . PHP_EOL;
            echo "\033[32m-- Tour de l'A.I. --\033[0m" . PHP_EOL . PHP_EOL;
            tour_ai_rand_allumettes();
            if($nb_allum == 0) {
                rejouer();
            }
        }
    } 
}

joueur_vs_ai_plusieurs_allumettes();
