<?php

function prendre_allumette()
{
    global $nb_allum;
    $allum_prise = trim(fgets(STDIN));  //Nb d'allumette que prend le joueur à un tour de jeu
    $allum_prise = intval($allum_prise); // Met le nombre sous forme d'entier

    if ($allum_prise == 1 || $allum_prise == 2 || $allum_prise == 3) {
        if ($allum_prise > $nb_allum) {
            echo "\033[34mIl ne reste pas suffisamment d\'allumettes.\033[0m" . PHP_EOL;
            echo "\033[34mRejoues, tu peux prendre max " . $nb_allum . " : \033[0m" . PHP_EOL;
            prendre_allumette();
        } else {
            $nb_allum = $nb_allum - $allum_prise;
            if ($nb_allum == 0) {
                echo "\033[34mIl n'y a plus d'allumette ! \033[0m";
            } else {
                echo "\033[34mIl reste " . $nb_allum . " allumettes.\033[0m" . PHP_EOL . PHP_EOL;
            }
        }
    } else {
        echo "\033[34mHey! Tu ne peux prendre qu'1 à 3 allumettes." . PHP_EOL;
        echo "Retente ta chance, prends 1, 2 ou 3 allumettes : \033[0m";
        prendre_allumette();
    }
}
