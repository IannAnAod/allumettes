<?php

function rejouer(){
    echo "\033[34mVeux-tu rejouer ? OUI / NON \033[0m";
    $reponse = trim(fgets(STDIN));

    if ($reponse == 'oui') {
        echo "\033[34mTu m'as répondu oui, malheuresement pour le moment je ne possède pas encore cette fonction. A bientôt.\033[0m" . PHP_EOL;
        joueur_vs_ai_plusieurs_allumettes();
    } else if ($reponse == 'non') {
        echo "\033[34mOk. A bientôt !\033[0m" . PHP_EOL;    
    } else {
        echo "\033[34mC'est oui ou c'est non ?\033[0m" . PHP_EOL; 
        rejouer();
    }
}