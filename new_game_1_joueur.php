<?php

echo PHP_EOL . "\033[31m******   Commencer une nouvelle partie   ******\033[0m" . PHP_EOL . PHP_EOL;

echo "\033[34mSalut nouveau joueur! Quel est ton nom ? \033[0m";
echo PHP_EOL;
$humain = trim(fgets(STDIN)); // Le premier joueur, réel


echo "\033[34mBienvenu! Voici la règle du jeu :" . PHP_EOL . PHP_EOL;
echo "=====================================================" . PHP_EOL;
echo "Il y a 11 allumettes.".PHP_EOL.'Tu en pioches 1, 2 ou 3, puis c\'est au joueur suivant.' . PHP_EOL . "Celui qui prend la dernière perd la partie." . PHP_EOL;
echo "=====================================================\033[0m" . PHP_EOL . PHP_EOL;