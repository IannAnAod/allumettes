<?php
$allumette = "| ";      // Représentation d'une allumette
$nb_allum = 11;         // Nb d'allumettes en jeu (11 en début de partie)

// Boucle qui affiche les allumettes
function allumettes_restantes()
{
    global $nb_allum;
    global $allumette;
    for ($i = 0; $i < $nb_allum; $i++) {
        echo $allumette;
    }
}
